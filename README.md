# MAODV - QoS AODV

**Implementasi NS2 dari Paper "QAODV: An AODV Based Routing Protocol for QoS Parameters"**

     Muhammad Adib Arinanda
     5115100111
     Kelas Jaringan Nirkabel 2018
---
#### Penjelasan singkat modifikasi AODV
Paper berjudul **QAODV: An AODV Based Routing Protocol for QoS Parameters** ini mengusulkan modifikasi AODV berbasis QoS (Quality of Service), dimana rute dari node source ke destination yang dipilih bukan hanya rute dengan hop count minimal, melainkan juga mempertimbangkan faktor delay dan available bandwidth dari rute yang akan dipilih tersebut. Pada paper tersebut, yang dimaksud delay adalah delay yang terdapat di masing-masing node ditambah link delay yang terdapat di masing-masing link di suatu rute. Sedangkan available bandwidth pada suatu node adalah kapasitas bandwidth suatu node dikurang bandwidth yang dibutuhkan oleh seluruh tetangga node tersebut. Rute yang akan dipilih oleh modifikasi AODV berbasis QoS ini adalah rute dengan hop count minimal, delay minimal, dan available bandwidth maksimal sehingga rute tersebut dapat dikatakan memenuhi parameter-parameter QoS.

#### [WIP] Modifikasi yang akan dilakukan 

1. Mengubah format paket dan struktur tabel routing
2.
3.